#### Cloud Project

repository: https://gitlab.com/twn-devops-bootcamp/latest/05-cloud/cloud-basics-exercises

EXERCISE 0: Clone Git Repository

Create a simple NodeJS app that lists all the projects each developer is working on and make it available online, so everyone can access it.

Clone the git repository and project/git repo from it


EXERCISE 1: Package NodeJS App
To have just 1 file, create an artifact from the Node App.Do the following:

Package your Node app into a tar file (npm pack)


EXERCISE 2: Create a new server
Your company uses DigitalOcean as Infrastructure as a Service platform, instead of having on-premise servers. So you:

Create a new droplet server on DigitalOcean


EXERCISE 3: Prepare server to run Node App
Now you have a new fresh server nothing installed on it. Because you want to run a NodeJS application you need to install Node and npm on it:

Install nodejs & npm on it


EXERCISE 4: Copy App and package.json
Having everything prepared for the application, you finally:

Copy your simple Nodejs app tar file and package.json to the droplet


EXERCISE 5: Run Node App
Start the node application in detached mode (npm install and node server.js commands)


EXERCISE 6: Access from browser - configure firewall
You see that the application is not accessible through the browser, because all ports are closed on the server. So you:

Open the correct port on Droplet
and access the UI from browser


